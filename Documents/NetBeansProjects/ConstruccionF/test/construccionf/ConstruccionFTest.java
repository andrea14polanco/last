/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package construccionf;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yahaira
 */
public class ConstruccionFTest {
    
    public ConstruccionFTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class ConstruccionF.
     */
    @Test
    public void testSum() {
         ConstruccionF c = new  ConstruccionF();
        int n = 6;   
        int m = 5;
        assertEquals(c.sum(n, m) , 11);
    }
    
}
